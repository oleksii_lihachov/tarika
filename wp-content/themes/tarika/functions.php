<?php
/**
 * tarika functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package tarika
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'tarika_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tarika_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on tarika, use a find and replace
		 * to change 'tarika' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'tarika', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'tarika' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'tarika_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height' => 250,
				'width' => 250,
				'flex-width' => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'tarika_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tarika_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'tarika_content_width', 640 );
}

add_action( 'after_setup_theme', 'tarika_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tarika_widgets_init() {
	register_sidebar(
		array(
			'name' => esc_html__( 'Sidebar', 'tarika' ),
			'id' => 'sidebar-1',
			'description' => esc_html__( 'Add widgets here.', 'tarika' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		)
	);
}

add_action( 'widgets_init', 'tarika_widgets_init' );

/**
 * Update jQuery to 3.3.1
 */
function sof_load_scripts() {
	if ( ! is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_enqueue_script(
			'jquery',
			'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
			'3.3.1',
			null,
			true
		);
	}
}

add_action( 'wp_enqueue_scripts', 'sof_load_scripts' );

/**
 * Enqueue scripts and styles.
 */
function tarika_scripts() {
	// Slick slider
	wp_enqueue_style(
		'slick-slider',
		get_stylesheet_directory_uri() . '/assets/vendor/slick/slick.css',
		array(),
		null
	);

	wp_enqueue_script(
		'slick-slider',
		get_stylesheet_directory_uri() . '/assets/vendor/slick/slick.min.js',
		array( 'jquery' ),
		null,
		true
	);

	// Main
	wp_enqueue_style(
		'tarika-style',
		get_stylesheet_directory_uri() . '/dist/style.css',
		array(),
		_S_VERSION
	);

	wp_enqueue_script(
		'tarika-scripts',
		get_stylesheet_directory_uri() . '/dist/bundle.js',
		array( 'jquery', 'slick-slider' ),
		_S_VERSION,
		true
	);

	// Add ajax-url to main script
	wp_localize_script( 'tarika-scripts',
		'user_info', array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'tarika_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Admin Options Page.
 */
require_once get_template_directory() . '/inc/options-page.php';
// Activate
new Options_Page;

/**
 * Add menu short code
 */
function menu_function( $atts, $content = null ) {
	extract(
		shortcode_atts(
			array( 'name' => null, ),
			$atts
		)
	);
	return wp_nav_menu(
		array(
			'container' => false,
			'menu' => $atts['name'],
			'menu_class' => $atts['class'],
			'echo' => false
		)
	);
}

add_shortcode( 'menu', 'menu_function' );

/**
 * Manually create excerpt from passed text
 *
 * @param $content
 * @param $length
 * @return bool|string
 */
function generate_excerpt_from_content( $content, $length = 200 ) {
	$excerpt = strip_shortcodes( $content );
	$excerpt = strip_tags( $excerpt );
	$excerpt = substr( $excerpt, 0, $length ) . '...';

	return $excerpt;
}

function cc_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

add_filter( 'upload_mimes', 'cc_mime_types' );

// Enable shortcodes in text widgets
add_filter( 'widget_text', 'do_shortcode' );