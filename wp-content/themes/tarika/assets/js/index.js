import '../scss/style.scss';

import './modules/menu';
import './modules/more-toggler';
import './modules/back-to-top';
import './modules/sliders';
import './modules/smooth-anchor-scroll';
import './modules/accordion';
