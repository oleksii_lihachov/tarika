export default jQuery(document).ready(function ($) {
	const $button = $('.js-hook__back-to-top');

	if ($button.length) {
		$button.on('click', function (e) {
			e.preventDefault();
			window.scrollTo({top: 0, behavior: 'smooth'});
		});
	}
});