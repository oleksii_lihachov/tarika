import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import { debounce } from "lodash";

const whichTransitionEvent = () => {
  let t;
  const el = document.createElement('fakeelement');
  const transitions = {
    'transition': 'transitionend',
    'OTransition': 'oTransitionEnd',
    'MozTransition': 'transitionend',
    'WebkitTransition': 'webkitTransitionEnd'
  };

  for (t in transitions) {
    if (el.style[t] !== undefined) {
      return transitions[t];
    }
  }
};


export default jQuery(document).ready(function ($) {
  const $header = $('.header');
  const $page = $('#page');
  const $hamburger = $('.hamburger');
  const $nav = $('.navigation');
  const menu = document.querySelector('.menu');
  const $window = $(window);
  let isLocked = false;
  const transitionEnd = whichTransitionEvent();

  $hamburger.on('click', function () {
    if (isLocked === false) {
      disableBodyScroll($nav.get(0));
      isLocked = true;
    } else {
      enableBodyScroll($nav.get(0));
      isLocked = false;
    }

    $header.toggleClass('open');
    $hamburger.toggleClass('active');
    $nav.toggleClass('open');
  });

  const autoHideHeaderOnScroll = () => {
    const elSelector = '.header',
      element = document.querySelector(elSelector);

    if (!element) return true;

    let elHeight = element.offsetHeight,
      wScrollCurrent = window.pageYOffset,
      wScrollBefore = 0,
      wScrollDiff = wScrollBefore - wScrollCurrent,
      elTop = parseInt(window.getComputedStyle(element).getPropertyValue('top')) + wScrollDiff;


    function autoHide(currentScroll, scrollDiff, elTop) {
      const distance = $(elSelector).offset().top;

      // scrolled to the very top; element sticks to the top
      if (currentScroll <= 0) {
        element.style.top = '0px';

        // scrolled up; element slides in
      } else if (scrollDiff > 0) {
        element.style.top = (elTop > 0 ? 0 : elTop) + 'px';

        // scrolled down
      } else if (scrollDiff < 0) {
        element.style.top = (Math.abs(elTop) > elHeight ? -elHeight : elTop) + 'px';
      }

      wScrollBefore = currentScroll;
      if (Math.abs(distance) <= elHeight) {
        $header.addClass('on-top');
      } else {
        $header.removeClass('on-top');
      }
    }

    // Calculate on first load
    autoHide(
      window.pageYOffset,
      wScrollBefore - wScrollCurrent,
      elTop
    );

    $window.on('scroll', function () {
      let wScrollCurrent = window.pageYOffset;
      let wScrollDiff = wScrollBefore - wScrollCurrent;
      let elTop = parseInt(window.getComputedStyle(element).getPropertyValue('top')) + wScrollDiff;

      autoHide(wScrollCurrent, wScrollDiff, elTop);
    });
  };

  const addPagePadding = debounce(function () {
    let calculatedPadding = $header.height();
    $page.css('padding-top', calculatedPadding + 'px');

    isLocked = false;
    enableBodyScroll($nav.get(0));
    $header.removeClass('open');
    $hamburger.removeClass('active');
    $nav.removeClass('open');
  });

  autoHideHeaderOnScroll();
  addPagePadding();
  $(window).on('resize', addPagePadding);

  menu.addEventListener('transitionstart', (ev) => {
    if (ev.target.classList.contains('menu') && ev.propertyName === 'left') {
      menu.classList.remove('transitionend');
    }
  }, false);

  menu.addEventListener(transitionEnd, (ev) => {
    if (ev.target.classList.contains('menu') && ev.propertyName === 'left') {
      menu.classList.add('transitionend');
    }
  }, false);
});